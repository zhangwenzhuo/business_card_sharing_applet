// pages/add/add.js
var arr=Array();
var app=getApp();
arr[0]="1";
arr[1]="2";
arr[2]="";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: 0,
    userInfo:null,
    index:'',
    indexs:2,
    underindex:0,
    imagepath: '',
    items:[
      { item:'职位'},
      { item: '公司' },
    ],
    movies: [
      { url: '../../img/swiper/2.jpg' },
      { url: '../../img/swiper/1.jpg' }
    ] ,
    array: ['部门', '固话', '电话','邮箱','传真','地址','网址','网店','邮编','微信','QQ','微博','公众号'],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  chooseimage: function (e) {
    var that = this;
    wx.chooseImage({
      success: function (res) {

        that.setData({
          imagepath: res.tempFilePaths[0],
          flag: 1
        })
      },
    })
  },
  submit:function(e)
  {
    console.log(e);
    var url=app.globalData.add+`?name=${e.detail.value.name}`+`&phone=${e.detail.value.phone}`+`&occupation=${e.detail.value.职位}`+`&company=${e.detail.value.公司}`+`&section=${e.detail.value.部门}`+`&telephone=${e.detail.value.固话}`+`&phones=${e.detail.value.电话}`+`&email=${e.detail.value.邮箱}`+`&fax=${e.detail.value.传真}`+`&address=${e.detail.value.地址}`+`&webadd=${e.detail.value.网址}`+`&shop=${e.detail.value.网店}`+`&wechat=${e.detail.value.微信}`+`&tencent=${e.detail.value.QQ}`+`&microblog=${e.detail.value.微博}`+`&public=${e.detail.value.公众号}`+`&openid=${app.globalData.openid}`;
    if(!e.detail.value.name)
    {
      wx.showModal({
        title: '提示',
        content: '请输入姓名',
      })
    }
if(!e.detail.value.phone)
    {
      wx.showModal({
        title: '提示',
        content: '请输入手机号',
      })
    }
    if(e.detail.value.name&&e.detail.value.phone)
    {
      console.log("传数据");
      //将表单数据传入后台处理
 wx.request({
   url: url,
   success:function(res)
   {
     console.log("111");
    },
    fail:function(res)
    {
console.log("fail");
    }
 })
    }
  },
  addmore:function(e)
  {
   // console.log("test");
    var that=this;
    that.setData({
      underindex: e.detail.value
    })
    var arr = this.data.items;
    arr.splice(that.data.indexs, 1, { item: that.data.array[e.detail.value]});
  
    that.setData({
      items:arr,
      indexs:that.data.indexs+1
    })

  }
})