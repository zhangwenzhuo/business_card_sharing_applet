// pages/search/search.js
var app=getApp();
var config = require('../../config')
var util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
image:'',
items:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  recog:function(e){
    console.log("111");
    var that=this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        //util.showBusy('正在上传')
        var filePath = res.tempFilePaths[0]

        // 上传图片
        wx.uploadFile({
          url: config.service.uploadUrl,
          filePath: filePath,
          name: 'file',

          success: function (res) {
            //util.showSuccess('上传图片成功')
            res = JSON.parse(res.data)
            that.setData({
              image: res.data.imgUrl
            })
          },

          fail: function (e) {
            //util.showModel('上传图片失败')
          }
        })

      },
      fail: function (e) {
        console.error(e)
      }
    })

   

  },
  onLoad: function (options) {

  },
  add:function(e){
    console.log("2222");
    wx.navigateTo({
      url: '../add/add',
    })
  },

shibie:function(res){
  console.log(this.data.image);
  var that=this;
  var url = app.globalData.recog + `?image=${this.data.image}`;
  wx.request({
    url: url,
    success: function (res) {
      that.setData({
        items: res
      })
      console.log(res.data.words_result);
    }
  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})